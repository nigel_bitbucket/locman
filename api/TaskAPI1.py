from flask_restful import Resource
from flask import request
import logging as logger
import db
import json


class Task1(Resource):

    def get(self):
        logger.debug("Inside get method of Task")
        return 'Loc App Check 2', 200

