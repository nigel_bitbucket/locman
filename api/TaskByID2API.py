from flask_restful import Resource
import logging as logger
import db
import json
from flask import request


# Get Location by IDb
class TaskByID(Resource):

    def get(self, satId):
        sql = "select * from location where satid = %s"
        db.cur.execute(sql, satId, )

        result = db.cur.fetchall()

        # return the results!
        logger.debug("Inisde the get method of TaskById. TaskID = {}".format(satId))
        return json.dumps(result), 200


# Decommission Location
class TaskByID1(Resource):

    def get(self, satId):
        sql = "select * from location where satid = %s"
        db.cur.execute(sql, satId, )

        result = db.cur.fetchall()

        # return the results!
        logger.debug("Inisde the get method of TaskById. TaskID = {}".format(satId))
        return json.dumps(result), 200

    def put(self, satId):
        sql = "update location set status = 0 where satid = %s"
        db.cur.execute(sql, satId, )
        db.conn.commit()
        logger.debug("Inisde the put method of TaskByID. TaskID = {}".format(satId))
        return {"message": "Decommissioned Location, ID = {}".format(satId)}, 200


# Update location
class TaskByID2(Resource):

    def get(self, satId):
        sql = "select * from location where satid = %s"
        db.cur.execute(sql, satId, )

        result = db.cur.fetchall()

        # return the results!
        logger.debug("Inisde the get method of TaskById. TaskID = {}".format(satId))
        return json.dumps(result), 200

    def put(self, satId):
        perigee = request.json['Perigee']
        apogee = request.json['Apogee']
        eccentricity = request.json['Eccentricity']
        inclination = request.json['Inclination']
        period = request.json['Period']

        sql = "update location set perigee = %s,apogee = %s,eccentricity = %s,inclination = %s,period = %s where satid =%s"
        db.cur.execute(sql, (perigee, apogee, eccentricity, inclination, period, satId,))
        db.conn.commit()
        logger.debug("Inisde the put method of TaskByID. TaskID = {}".format(satId))
        return {"message": "Updated Satellite location | Satellite, ID = {}".format(satId)}, 200
