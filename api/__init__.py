from flask_restful import Api
from app import flaskAppInstance
from .TaskAPI import Task
from .TaskByID2API import TaskByID
from .TaskByID2API import TaskByID1
from .TaskByID2API import TaskByID2
from .TaskAPI1 import Task1

restServerInstance = Api(flaskAppInstance)

restServerInstance.add_resource(Task, "/api/v1.0/loc/")
restServerInstance.add_resource(TaskByID, "/api/v1.0/loc/<string:satId>")
restServerInstance.add_resource(TaskByID1, "/api/v1.0/loc/decommission/<string:satId>")
restServerInstance.add_resource(TaskByID2, "/api/v1.0/loc/update/<string:satId>")
restServerInstance.add_resource(Task1, "/api/v1.0/loc/check/")
