from flask_restful import Resource
from flask import request
import logging as logger
import db
import json


class Task(Resource):

    def get(self):
        db.cur.execute("select * from location")

        result = db.cur.fetchall()

        #       db.conn.close()
        # return the results!
        logger.debug("Inside get method of Task")
        return json.dumps(result), 200

    def post(self):
        locid = request.json['id']
        perigee = request.json['Perigee']
        apogee = request.json['Apogee']
        eccentricity = request.json['Eccentricity']
        inclination = request.json['Inclination']
        period = request.json['Period']
        status = request.json['active']
        satid = request.json['satid']

        sql = "insert into satellite values (%s,%s,%s,%s,%s,%s,%s,%s)"
        db.cur.execute(sql, (locid, perigee, apogee, status, eccentricity, inclination, period, status, satid))

        db.conn.commit()
        logger.debug("Inisde the post method of Task")
        return {"message": "Added New Location - {}".format(satid)}, 200
