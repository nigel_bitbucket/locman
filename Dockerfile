FROM ubuntu:bionic

RUN apt-get update -y && \
    apt-get install -y python3-pip python3-dev wget
#    && apt-get -yy install libmariadb-dev
	

# We copy just the requirements.txt first to leverage Docker cache 
COPY ./requirements.txt /app/requirements.txt
#COPY ./install_maria_connector.sh /app/install_maria_connector.sh

WORKDIR /app

#RUN chmod a+x install_maria_connector.sh && ./install_maria_connector.sh
#RUN apt-get install -y libmariadb-dev
# RUN apt-get install -y unixodbc-dev unixodbc-bin unixodbc
# RUN tail -f /dev/null
RUN pip3 install -r requirements.txt


#Copy the Code
COPY . /app

ENTRYPOINT [ "python3" ]

CMD [ "app.py" ]
